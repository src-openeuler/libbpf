%global githubname   libbpf
%global githubver    1.2.2
%global githubfull   %{githubname}-%{githubver}

Name:           %{githubname}
Version:        %{githubver}
Release:        6
Summary:        Libbpf library

License:        LGPLv2 or BSD
URL:            https://github.com/%{githubname}/%{githubname}
Source:         https://github.com/%{githubname}/%{githubname}/archive/refs/tags/v%{githubver}.tar.gz
BuildRequires:  gcc elfutils-libelf-devel elfutils-devel
BuildRequires:  make

Patch0000:      backport-libbpf-Ensure-FD-3-during-bpf_map__reuse_fd.patch
Patch0001:      backport-libbpf-Ensure-libbpf-always-opens-files-with-O_CLOEX.patch
Patch0002:      backport-libbpf-Set-close-on-exec-flag-on-gzopen.patch
Patch0003:      backport-libbpf-Fix-NULL-pointer-dereference-in_bpf_object__c.patch
Patch0004:	backport-libbpf-Free-btf_vmlinux-when-closing-bpf_object.patch
Patch0005:      backport-libbpf-Avoid-uninitialized-value-in-BPF_CORE_READ_BI.patch
Patch0006:      backport-libbpf-Add-NULL-checks-to-bpf_object__prev_map,next_.patch
Patch0007:      backport-libbpf-Apply-map_set_def_max_entries-for-inner_maps-.patch
Patch0008:      backport-libbpf-Dont-take-direct-pointers-into-BTF-data-from-.patch

# This package supersedes libbpf from kernel-tools,
# which has default Epoch: 0. By having Epoch: 1
# this libbpf will take over smoothly
Epoch:          2

%description
A mirror of bpf-next linux tree bpf-next/tools/lib/bpf directory plus its
supporting header files. The version of the package reflects the version of
ABI.

%package devel
Summary:        Development files for %{name}
Requires:       %{name} = 2:%{version}-%{release}
Requires:       kernel-headers >= 5.16.0
Requires:       zlib
%description devel
The %{name}-devel package contains libraries header files for
developing applications that use %{name}

%package static
Summary: Static library for libbpf development
Requires: %{name}-devel = 2:%{version}-%{release}

%description static
The %{name}-static package contains static library for
developing applications that use %{name}

%global make_flags DESTDIR=%{buildroot} OBJDIR=%{_builddir} CFLAGS="%{build_cflags} -fPIC -Werror" LDFLAGS="%{build_ldflags} -Werror -Wl,--no-as-needed -Wl,-z,noexecstack" LIBDIR=/%{_libdir} NO_PKG_CONFIG=1

%prep
%autosetup -n %{githubfull} -p1

%build
%make_build -C ./src %{make_flags}

%install
%make_install -C ./src %{make_flags}

%files
%{_libdir}/libbpf.so.%{githubver}
%{_libdir}/libbpf.so.1

%files devel
%{_libdir}/libbpf.so
%{_includedir}/bpf/
%{_libdir}/pkgconfig/libbpf.pc

%files static
%{_libdir}/libbpf.a

%changelog
* Mon Dec 23 2024 zhangmingyi <zhangmingyi5@huawei.com> 2:1.2.2-6
- backport patch from upstream:
  backport-libbpf-Dont-take-direct-pointers-into-BTF-data-from-.patch
  
* Wed Oct 09 2024 zhangmingyi <zhangmingyi5@huawei.com> 2:1.2.2-5
- backport patch from upstream:
  backport-libbpf-Add-NULL-checks-to-bpf_object__prev_map,next_.patch
  backport-libbpf-Apply-map_set_def_max_entries-for-inner_maps-.patch

* Wed Sep 25 2024 zhangmingyi <zhangmingyi5@huawei.com> 2:1.2.2-4
- backport patch from upstream:
  backport-libbpf-Avoid-uninitialized-value-in-BPF_CORE_READ_BI.patch

* Fri May 10 2024 jinzhiguang <jinzhiguang@kylinos.cn> 2:1.2.2-3
- backport patch from upstream:
  backport-libbpf-Free-btf_vmlinux-when-closing-bpf_object.patch

* Mon Apr 15 2024 zhangmingyi <zhangmingyi5@huawei.com> 2:1.2.2-2
- backport patches from upstream:
  backport-libbpf-Fix-NULL-pointer-dereference-in_bpf_object__c.patch

* Fri Jan 19 2024 zhangmingyi <zhangmingyi5@huawei.com> 2:1.2.2-1
- update to version 1.2.2

* Thu Dec 7 2023 zhangmingyi <zhangmingyi5@huawei.com> 2:0.8.1-11
- backport patches from upstream:
  backport-libbpf-Fix-realloc-API-handling-in-zero-sized-edge-cases.patch
  backport-libbpf-Set-close-on-exec-flag-on-gzopen.patch
  backport-libbpf-Fix-is_pow_of_2.patch
  backport-libbpf-make-RINGBUF-map-size-adjustments-more-eagerly.patch

* Mon Aug 14 2023 zhangmingyi <zhangmingyi5@huawei.com> 2:0.8.1-10
- backport patches from upstream:
  backport-libbpf-Ensure-FD-3-during-bpf_map__reuse_fd.patch
  backport-libbpf-Ensure-libbpf-always-opens-files-with-O_CLOEX.patch

* Fri Aug 4 2023 JofDiamonds <kwb0523@163.com> -2:0.8.1-9
- sync bpf helper funcs from kernel

* Sat May 13 2023 zhangmingyi <zhangmingyi5@huawei.com> -2:0.8.1-8
- backport patches from upstream:
  backport-libbpf-disassociate-section-handler-on-explicit-bpf_.patch
  backport-libbpf-Use-correct-return-pointer-in-attach_raw_tp.patch
  backport-libbpf-Use-elf_getshdrnum-instead-of-e_shnum.patch

* Fri May 12 2023 zhangmingyi <zhangmingyi5@huawei.com> -2:0.8.1-7
- backport patches from upstream:
  backport-libbpf-Disable-SEC-pragma-macro-on-GCC.patch

* Fri Apr 28 2023 SuperCharge <xiesongyang@huawei.com> -2:0.8.1-6
- backport patches from upstream:
  backport-libbpf-Fix-alen-calculation-in-libbpf_nla_dump_error.patch
  
* Thu Apr 20 2023 zhangmingyi <zhangmingyi5@huawei.com> -2:0.8.1-5
- add -Werror -Wl,--no-as-needed options

* Sat Mar 11 2023 SuperCharge <xiesongyang@huawei.com> -2:0.8.1-4
- backport patches from upstream:
  backport-libbpf-Fix-overrun-in-netlink-attribute-iteration.patch
  backport-libbpf-Fix-use-after-free-in-btf_dump_name_dups.patch
  backport-libbpf-Deal-with-section-with-no-data-gracefully.patch
  backport-libbpf-Fix-null-pointer-dereference-in-find_prog_by_.patch
  backport-sync-start-syncing-include-uapi-linux-fcntl.h-UAPI-h.patch
  backport-libbpf-Handle-size-overflow-for-ringbuf-mmap.patch
  backport-libbpf-Use-page-size-as-max_entries-when-probing-rin.patch

* Fri Jan 6 2023 zhangmingyi<zhangmingyi5@huawei.com> - 2:0.8.1-3
-- backporting: backport-libbpf-Ensure-functions-with-always_inline-attribute-are-inline.patch 
		backport-libbpf-Fix-the-name-of-a-reused-map.patch 
		backport-libbpf-preserve-errno-across-pr_warn-pr_info-pr_debug.patch

* Fri Dec 2 2022 zhangmingyi<zhangmingyi5@huawei.com> - 2:0.8.1-2
- update libbpf.spec source

* Fri Dec 2 2022 zhangmingyi<zhangmingyi5@huawei.com> - 2:0.8.1-1 
- update master

* Mon Aug 22 2022 hujiawang<hujiawang1@guawei.com> - 2:0.3-3
- Change the package name

* Wed Aug 3 2022 Lv Ying<lvying6@huawei.com> - 2:0.3-2
- support detecting and attaching of writable tracepoint program
  https://lore.kernel.org/bpf/20211004094857.30868-3-hotforest@gmail.com

* Mon Dec 27 2021 sunsuwan<sunsuwan2@huawei.com> - 2:0.3-1
- update libbpf from v0.1.1 to v0.3

* Sun Sep 26 2021 zhudi<zhudi2@huawei.com> - 0.1.1-1.h1
- Type:bugfix
- CVE:
- SUG:restart
- DESC: add Use SOCK_CLOEXEC when opening the netlink socket

* Wed Oct 28 2020 hubble_zhu <hubble_zhu@qq.com> - 0.1.1-1
- update libbpf from v0.0.6 to v0.1.1

* Sun Apr 26 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.0.6-1
- Package init
